use DB_GRUPO_DEL_LLANO

/*PERFILES
- CARACTERISTICAS PARA EJECUTAR ACCIONES - ADMIN - USUARIO - TECNICO

ROL
- ACCION A EJECUTA EL USUARIO- CRUD*/


/*1.  Elaborar una consulta que permita identificar los datos basicos de las personas asociadas 
a los usuarios , el nombre de las empresa , los programas y las opciones sobre los 
programas de los usuarios que hayan estado activos durante un interlao de tiempo .*/

SELECT 	 IU.INFUSU_NOM		AS NOMBRE
		,IU.INFUSU_APE		AS APELLIDO
		,IU.INFUSU_TIPDOC	AS [TIPO DOCUMENTO]
		,IU.INFUSU_DOC		AS [NUMERO DOCUMENTO]
		,EM.EMP_NOM			AS [NOMBRE EMPRESA]
		,PE.PERMDES			AS [DESCRIPCION DEL PERMISO]
		,PR.PROGDES			AS [DESCRIPCION DEL PROGRAMA]
FROM USUARIO AS U
INNER JOIN INFO_USU 		AS IU ON IU.ID				= U.USU_INFO_ID
INNER JOIN EMPRESA 			AS EM ON EM.ID				= U.USU_EMP_ID
INNER JOIN USU_PERMISOS 	AS UP ON UP.USUPERM_USU_ID	= U.ID
INNER JOIN PERMISOS 		AS PE ON PE.ID				= UP.USUPERM_PERM_ID
INNER JOIN PROGRAMAS 		AS PR ON PR.ID				= PE.PERMPROG_ID
WHERE 	U.USU_EST = 1
AND 	U.USU_VIG BETWEEN 'YYYYMMDD' AND 'YYYYMMDD'

/*2. ELaborar una Consulta que agrupe la informaci�n por empresa y logins mostrando la 
cantidad de programas con permiso de eliminacion que tengan los usuarios que estan 
activos . */

SELECT   EM.EMP_NOM				AS [NOMBRE EMPRESA]
		,U.USERNAME				AS [LOGINS]
		,COUNT(PE.PERMPROG_ID)	AS [CANTIDAD DE PROGRAMAS CON PERMISOS DE ELIMINACION]
FROM EMPRESA 				AS EM
INNER JOIN USUARIO 			AS U	ON EM.ID				= U.USU_EMP_ID
INNER JOIN USU_PERMISOS 	AS UP	ON UP.USUPERM_USU_ID	= U.ID
INNER JOIN PERMISOS 		AS PE	ON PE.ID				= UP.USUPERM_PERM_ID
WHERE	U.USU_EST	= 1
AND		PE.PERMDESC = 'DELETE'
GROUP BY EM.EMP_NOM, U.USERNAME

/*3. Se deber� construir un update masivo que permita inactivar usuarios que esten 
accediendo a un programa especifico*/
UPDATE	USUARIO
SET		U.USU_EST = 0
FROM	USUARIO				AS U
INNER JOIN USU_PERMISOS 	AS UP	ON UP.USUPERM_USU_ID	= U.ID
INNER JOIN PERMISOS 		AS PE	ON PE.ID				= UP.USUPERM_PERM_ID
INNER JOIN PROGRAMAS 		AS PR	ON PR.ID				= PE.PERMPROG_ID
WHERE PR.PROGDESC = ''
OR PR.ID = ''

/*4. Crear un Trigger sobre la tabla de usuarios que se active ante el evento de update , el 
triger deber� insertar en una tabla de log , la fecha y hora del sistema en la que se hace el 
cambio , y debe dejar la evidencia del valor anterior y el nuevo valor*/
CREATE TRIGGER UserChangue
ON USUARIO
AFTER UPDATE
AS BEGIN 
   INSERT INTO LOG_AUDITORIA (LOGAUD_USU_ID,LOGAUD_FEC_USUI,LOGAUD_USU_ANTV,LOGAUD_USU_NUEV)
   VALUES (1,GETDATE(),'VIGENCIA ANTERIOR: 2022-10-27','VIGENCIA NUEVA: 2022-11-30')
END;

/*5. Establecer particionamiento Logico para el almacenamiento de los logs en estructuras 
distintas agrupando por a�os la informaci�n del log de auditoria , debe quedar un log que es 
el vigente y agrupados por a�os los registros de log de a�os anteriores al actual .*/

USE master;  
ALTER DATABASE DB_GRUPO_DEL_LLANO   
ADD LOG FILE   
(  
    NAME = VigenteLog,  
    FILENAME = 'D:\Microsoft SQL Server\MSSQL\DATA\VigenteLog.ldf',  
    SIZE = 5MB,  
    MAXSIZE = 100MB,  
    FILEGROWTH = 5MB  
);

/*6. Definir los indices requeridas para el m�delo a fin de que este se mantenga en optimas 
condiciones de operaci�n y rendimiento .*/
CREATE NONCLUSTERED INDEX [NonClusteredIndex_USUARIO]			ON [dbo].[USUARIO]			([ID] ASC,[USU_EMP_ID] ASC,[USU_PER_ID] ASC,[USU_INFO_ID] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_INFO_USU]			ON [dbo].[INFO_USU]			([INFUSU_DOC] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_USU_PERMISOS]		ON [dbo].[USU_PERMISOS]		([USU_PERM_USU_ID] ASC,[USU_PERM_PERM_ID] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_PERMISOS]			ON [dbo].[PERMISOS]			([PERMPROG_ID] ASC,[PERMDESC] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_PROGRAMAS]			ON [dbo].[PROGRAMAS]		([PROGDES] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_PERFIL_PROGRAMAS]	ON [dbo].[PERFIL_PROGRAMAS]	([PERFPROG_PROG] ASC,[PERFPROG_PERM] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_PERFIL]			ON [dbo].[PERFIL_PROGRAMAS]	([PERGDES] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_USU_PER]			ON [dbo].[USU_PER]			([USUPER_PER_ID] ASC,[USUPER_USU_ID] ASC)
CREATE NONCLUSTERED INDEX [NonClusteredIndex_LOG_AUDITORIA]		ON [dbo].[LOG_AUDITORIA]	([LOG_AUD_USU_ID] ASC,[LOG_AUD_PRO_ID] ASC)

/*7. Cree un procedimiento almacenado que utilice cursores para indicar por cada empresa 
que funcionalidades se han utilizado en cada empresa y retornar un listado donde se 
permitan visualizar las principales funcionalidades que se han utilizado por cada usuario en 
cada empresa estableciendo la cantidad de veces que se ha utilizado y cuales han sido de 
Eliminaci�n y cuales de Actualizaci�n .*/


/**/
/**/

